import { useDispatch, useSelector } from "react-redux";
import { deleteResult, storeResult } from "../../store/result.slice";

function Result() {
  const { result } = useSelector((store) => store.res);
  const { counter } = useSelector((store) => store.ctr);
  const dispatch = useDispatch();

  return (
    <>
      <h1>Result Component</h1>
      <button
        className="btn btn-warning"
        onClick={() => dispatch(storeResult(counter))}
      >
        Store Counter
      </button>
      <ul className="list-group">
        {result.map((res, index) => (
          <li
            onClick={() => dispatch(deleteResult(index))}
            className="list-group-item"
            key={index}
          >
            {res}
          </li>
        ))}
      </ul>
    </>
  );
}

export default Result;
