import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { userRegister } from "../../store/user.slice";
import Profile from "../Profile/Profile";

function User() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { isLoading, error, token } = useSelector((store) => store.user);
  const dispatch = useDispatch();

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(userRegister({ email, password }));
  };

  if (isLoading) {
    return <h2>Loading...</h2>;
  }

  if (error) {
    return <h2>{error.message}</h2>;
  }

  if (token) {
    return <Profile />;
  }
  return (
    <>
      <div className="row">
        <div className="col-6 offset-3">
          <h1>User Register</h1>
          <form onSubmit={submitHandler}>
            {/* email */}
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                name="email"
                id="email"
                placeholder=""
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <label htmlFor="email">Email</label>
            </div>

            {/* password */}
            <div className="form-floating mb-3">
              <input
                type="password"
                className="form-control"
                name="password"
                id="password"
                placeholder=""
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <label htmlFor="password">Password</label>
            </div>

            {/* button */}
            <button type="submit" className="btn btn-primary">
              Register
            </button>
          </form>

          {token && <h4>User created successfully!</h4>}
        </div>
      </div>
    </>
  );
}

export default User;
