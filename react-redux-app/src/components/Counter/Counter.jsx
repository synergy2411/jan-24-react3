import { useSelector } from "react-redux";
import CounterButtons from "./CounterButtons";

function Counter() {
  const counter = useSelector((store) => store.ctr.counter);
  return (
    <>
      <h1>Counter : {counter}</h1>
      <CounterButtons />
    </>
  );
}

export default Counter;
