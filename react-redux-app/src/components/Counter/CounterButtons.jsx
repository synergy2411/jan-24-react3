import { useDispatch } from "react-redux";
import { increment, decrement } from "../../store/counter.slice";

function CounterButtons() {
  const dispatch = useDispatch();

  return (
    <div className="btn-groups">
      <button className="btn btn-primary" onClick={() => dispatch(increment())}>
        Increase
      </button>
      <button
        className="btn btn-secondary"
        onClick={() => dispatch(decrement())}
      >
        Decrease
      </button>
    </div>
  );
}

export default CounterButtons;

// action : {type : "", payload? :""}
