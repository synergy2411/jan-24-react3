import { useDispatch } from "react-redux";
import { userLogout } from "../../store/user.slice";

function Profile() {
  const dispatch = useDispatch();
  return (
    <>
      <h1>Profile Page</h1>
      <button
        className="btn btn-outline-danger"
        onClick={() => {
          dispatch(userLogout());
        }}
      >
        Logout
      </button>
    </>
  );
}

export default Profile;
