import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  result: [],
};

const resultSlice = createSlice({
  name: "result",
  initialState,
  reducers: {
    storeResult(state, action) {
      state.result = [action.payload, ...state.result];
    },
    deleteResult(state, action) {
      state.result = state.result.filter(
        (res, index) => index !== action.payload
      );
    },
  },
});

export const { storeResult, deleteResult } = resultSlice.actions;

export default resultSlice.reducer;
