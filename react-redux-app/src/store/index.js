import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "./counter.slice";
import resultReducer from "./result.slice";
import userReducer from "./user.slice";

const store = configureStore({
  reducer: {
    ctr: counterReducer,
    res: resultReducer,
    user: userReducer,
  },
});

export default store;

// store = { todos : [], counter : 0, result : [], token : null}
