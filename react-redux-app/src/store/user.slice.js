import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { initializeApp } from "firebase/app";
import {
  createUserWithEmailAndPassword,
  getAuth,
  signOut,
} from "firebase/auth";

const app = initializeApp({
  apiKey: "AIzaSyCZO6oyZAHeoBcbc1yJ8mpnMFUpvTFtRTo",
  authDomain: "jan-24-react.firebaseapp.com",
});

const auth = getAuth(app);

export const userRegister = createAsyncThunk(
  "user/register",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      const userCredentails = await createUserWithEmailAndPassword(
        auth,
        email,
        password
      );
      return userCredentails.user.getIdToken();
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const userLogout = createAsyncThunk(
  "user/logout",
  async (args, { rejectWithValue }) => {
    try {
      await signOut(auth);
    } catch (err) {
      return rejectWithValue({ message: "Unable to sign out" });
    }
  }
);

// export const userRegister = createAsyncThunk(
//   "user/register",
//   async ({ email, password }, { rejectWithValue }) => {
//     try {
//       const response = await fetch("http://localhost:3030/users", {
//         method: "POST",
//         body: JSON.stringify({ email, password }),
//         headers: {
//           "Content-Type": "application/json",
//         },
//       });
//       return await response.json();
//     } catch (err) {
//       return rejectWithValue({ message: "Unable to create user" });
//     }
//   }
// );

const initialState = {
  error: null,
  isLoading: null,
  createdUser: null,
  token: null,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  extraReducers: (builder) => {
    builder.addCase(userRegister.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(userRegister.fulfilled, (state, action) => {
      state.isLoading = false;
      // state.createdUser = action.payload;
      console.log("TOKEN : ", action.payload);
      state.token = action.payload;
    });
    builder.addCase(userRegister.rejected, (state, action) => {
      state.isLoading = false;
      state.createdUser = null;
      state.error = action.payload;
    });

    builder.addCase(userLogout.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(userLogout.fulfilled, (state, action) => {
      state.isLoading = false;
      state.token = null;
    });
    builder.addCase(userLogout.rejected, (state, action) => {
      state.error = action.payload;
      state.isLoading = false;
    });
  },
});

export default userSlice.reducer;

// Promise -> Pending -> Settled (fulfilled / rejected)
