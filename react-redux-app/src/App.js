import Counter from "./components/Counter/Counter";
import Result from "./components/Result/Result";
import User from "./components/User/user";

function App() {
  return (
    <div className="container">
      <h1>App works</h1>

      <User />
      {/* <Counter />
      <Result /> */}
    </div>
  );
}

export default App;
