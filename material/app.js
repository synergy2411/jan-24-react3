// var x = "Hello";

// console.log(typeof x);      // string

// x = 201;

// console.log(typeof x);      // number

// function longRunningOp() {
//   setTimeout(function () {
//     console.log("Running the operation");
//   }, 1500);
// }

// function webRequest(req) {
//   console.log("Start with ", req.id);
//   longRunningOp();
//   console.log("End with ", req.id);
// }

// webRequest({ id: "r001" });
// webRequest({ id: "r002" });

// start with r001
// end with r001
// start with r002
// end with r002

// after 1.5s
// running operation
// running operation

// function demoAsync() {
//   console.log("START");

//   setTimeout(function () {
//     console.log("TIMEOUT");
//   }, 0);

//   Promise.resolve("PROMISE").then((data) => console.log(data));

//   console.log("END");
// }

// demoAsync();

// start
// end
// PROMISE
// TIMEOUT

function greet() {
  console.log("Hello");
}

function demoGreet(cb) {
  cb();
}

demoGreet(greet); // HoF

function demoGreetAgain() {
  return () => "Do something";
}

const nestedFn = demoGreetAgain(); // HoF
