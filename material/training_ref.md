# Break Timing

- Tea Break : 11:00AM (15 mins)
- Lunch : 1:00PM (45 mins)
- Tea Break 3:30PM (15 mins)

# JavaScript

- Dynamic Language : Types are determined at runtime
- Loosely typed
- Scripting Language
- Asynchronous : task executed in next cycle of code execution
- ES6+ features : OOPS - classes / object, Arrow function, Destructuring, Rest,spread, template syntax; Object.assign(), values(), entries(), getOwnPropertyDescriptor, padding function etc (BABEL)
- Event based
- Client (Browser) as well as Server side (Node Platform)
- Compiled (AST -> Abstract Syntax Tree, JIT Compiler) & Interpreted
- Single threaded / Non-blocking
- OOPS concepts / Object based / Functional Programming
- Various Design Patterns

# JavaScript Types

- Primitive : number, string, boolean, symbol, bigint
- Reference : object, array, date, function

# Full Stack Apps

- MEAN : MONGODB EXPRESS ANGULAR NODE
- MERN : MONGODB EXPRESS REACT NODE

# JavaScript Async behaviour

- All Synchronous code will execute first
- Asynchronous code will execute (in next cycle)
  > MicroTasks : Promises, queueMicrotask()
  > MacroTasks : all long running operation (eg. timer, read/write file, sockets)

# BABEL -> transpiler -> JSX into JavaScript

- nextgen JavaScript features, today
- ES13 -> ES6 / ES5

# JavaScript Libraries / Frameworks

- AngularJS : v1, Library; Components
- Angular : v2 onwards; Angular compiler, Framework; One-way data binding, Components based, Routing - SPA, Forms Validation, Guards : Routing Module, Lazy Loading : Webpack, Services - State Management, RxJS - Observable API, PB + EB = 2way; [(ngModel)]; MVC ; library, MFE etc

- jQuery : DOM manipulation, Animation, Remote Server Call (AJAX Calls)
- KnockoutJS : MVVM Pattern, 2 way data binding
- Backbone: CLient side MVC
- EmberJS : Framework; frequent API changes
- VueJS : Framework; No Corporate Care-taker; "Evan You"; still evolving

- React : light-weight (30kb); Library: View only; to render the UI quickly and efficiently

  > State Management : redux, Redux Toolkit
  > SPA : react-router-dom
  > Form Validation : formik, react-hook-form, yup etc
  > Lazy loading : webpack
  > RxJS Library for Observables
  > One way data-binding (state variable + callback (onChange))
  > Components
  > Vast community
  > Huge eco-system
  > GraphQL API
  > NextJS : Server-Side Framework for React
  > Gatsby
  > Native Apps

- Data binding : sync of variable data with view
- Data Flow : unidirectional

-> Parent Comp [data] -> Child Comp [Props]

# Web Design Principles - ATOMIC DESIGN

- Atom : one button, one input element
- Molecule : comb of atoms. eg. SearchBar -> one input element + One Button
- Organism : comb of molecules. eg. NavigationBar -> Various links + SearchBar
- Template : comb of organism. eg. Form
- Page : a complete web page

# Thinking in React way :

- if reusable piece of code, make it a component

# React CLI Tool

- npx create-react-app frontend
- cd frontend
- npm start

- npm install @babel/plugin-proposal-private-property-in-object -D
- npm install bootstrap

# JSX Restriction

- Should have only one root element
- Should not use javascript reserved keywords

# React Forms -

- Controlled Component
  > Form element value is controlled by the React state
  > State is updated on every change event
  > eg. Registration Form
- Uncontrolled Component
  > Form element value is controlled by the browser internal state
  > Component does not re-render for every change of value
  > "refs" props
  > eg. Login Form

# Side-effect - anything which is not meant by React

- making XHR Call
- SPA Apps
- Change the state

# Higher Order Function (HoF)

- function which receives other functions as parameter
- function which returns another function

# Higher Order Component (HoC)

- Component which receives other component as parameter
- Component which returns another Component
- Reuse the logic

Form - EmployeeInfoComponent -

> PersonalInfo : new Component
> ProfessionalInfo : new Component

---

# React Hooks

# useEffect : various flavours

- useEffect(cb) : cb executes at initial rendering as well as after every re-render of component

- useEffect(cb, []) :(componentDidMount) cb executes at ONLY initial rendering

- useEffect(cb, [dependency]) : (componentDidUpdate) cb executes at initial rendering as well as whenever the dependency changes

- useEffect(cb => cleanUpFn, [Deps]) -> (componentWillUnmount)
  > cb executes at initial rendering
  > whenever the dependency change, cleanUpFn will execute before the cb
  > cleanUpFn will execute before the component unmount

# Prop-drilling : unnecessary sending the prop to the in-between components

- Context API
- Component Composition

# useCallback(cb, []) : Does NOT create new callback in memory when the component re-render

- return the same cb function memoized (stored) value

# useMemo(cb => Value, []) : Does NOT create new Reference in memory, when the component re-render

- return the same value - memozied value

# JSON SERVER

> npm install json-server@0.17.4 -g
> json-server --version
> json-server --watch db.json --port=3030

# React Router

- Path <=> Component

- http://localhost:3000/home => HomeComp
- http://localhost:3000/expenses => ExpensesComponent

> npm install react-router-dom@6
> npm install bootstrap

- createBrowerRouter(path to component mapping)

# React Concepts

- Never update the state mutably
- When newState depends upon the oldState, use state changing function in setState
- Lifting-up the State (functional props)
- Data flow in one direction only
- State variable change will re-render the functional component
- State change in async mode while calling the setState function
- No side effects in Render phase of component life cycle

# State Management

- Component : useState; Component level state
- Context API : not efficient for frequent data change. eg. token, user preference - theme, colors, products
- Redux : App-wide data, changing frequently, huge, complex

# React Redux Libraries-

- npm install @reduxjs/toolkit react-redux bootstrap
- npm install redux react-redux bootstrap (for older version)
