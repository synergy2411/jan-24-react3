import React from "react";

const AuthContext = React.createContext({
  isLoggedIn: false,
  setIsLoggedIn: () => {},
  token: null,
  setToken: () => {},
});

export default AuthContext;
