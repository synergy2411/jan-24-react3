import { useState } from "react";
import Expenses from "./components/Expenses/Expenses";
import ClassBased from "./components/demo/ClassBased";
import CounterOne from "./components/demo/Hoc/CounterOne";
import Login from "./components/demo/login";
import Register from "./components/demo/RegisterForm";
import EffectDemo from "./components/demo/Hooks/EffectDemo";
import Auth from "./components/demo/Auth/Auth";

import AuthContext from "./context/auth-context";
import ReducerDemo from "./components/demo/Hooks/ReducerDemo";
import CallbackDemo from "./components/demo/Hooks/CallbackDemo";

function App() {
  const [isShow, setIsShow] = useState(true);
  // const [isLoggedIn, setIsLoggedIn] = useState(false);
  // const [token, setToken] = useState(false);

  return (
    <div className="container">
      <h1 className="text-center">App Component works!</h1>

      {/* <CallbackDemo /> */}
      {/* <ReducerDemo /> */}

      {/* <AuthContext.Provider
        value={{ isLoggedIn, setIsLoggedIn, token, setToken }}
      >
        <Auth />
      </AuthContext.Provider> */}

      <Expenses />

      {/* <EffectDemo /> */}

      {/* <Login /> */}
      {/* <Register /> */}
      {/* 
      <button className="btn btn-success" onClick={() => setIsShow(!isShow)}>
        Toggle Show
      </button>
      <ClassBased isShow={isShow} /> */}

      {/* <CounterOne />
      <CounterOne /> */}
    </div>
  );
}

export default App;
