import ExpenseDate from "../ExpenseDate/ExpenseDate";
import PropTypes from "prop-types";

const ExpenseItem = ({ id, title, amount, createdAt, onDelete }) => {
  return (
    <div className="col-4 mb-4">
      <div className="card">
        <div className="card-header">
          <h5 className="text-center">{title.toUpperCase()}</h5>
        </div>
        <div className="card-body">
          <p>Amount : INR {amount}</p>
          <ExpenseDate createdAt={createdAt} />

          <button
            className="btn btn-outline-danger btn-sm"
            onClick={() => onDelete(id)}
          >
            Delete
          </button>
        </div>
      </div>
    </div>
  );
};

export default ExpenseItem;

ExpenseItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  createdAt: PropTypes.instanceOf(Date),
  onDelete: PropTypes.func.isRequired,
};
