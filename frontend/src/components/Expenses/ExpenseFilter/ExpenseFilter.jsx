const ExpenseFilter = ({ onYearSelect, selectedYear }) => {
  return (
    <div className="mb-3">
      <select
        className="form-select form-select-lg"
        onChange={(e) => onYearSelect(e.target.value)}
        value={selectedYear}
      >
        <option value="">All Expenses</option>
        <option value="2022">2022</option>
        <option value="2023">2023</option>
        <option value="2024">2024</option>
      </select>
    </div>
  );
};

export default ExpenseFilter;
