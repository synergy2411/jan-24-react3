import AddExpense from "./AddExpense/AddExpense";
import ExpenseFilter from "./ExpenseFilter/ExpenseFilter";
import ExpenseItem from "./ExpenseItem/ExpenseItem";
import { useState, useEffect } from "react";
import useFetch from "../../Hooks/fetch";

const Expenses = () => {
  const [data] = useFetch("http://localhost:3030", "expenses");

  const [expenses, setExpenses] = useState([]);

  useEffect(() => {
    setExpenses(
      data.map((exp) => {
        return {
          ...exp,
          amount: Number(exp.amount),
          createdAt: new Date(exp.createdAt),
        };
      })
    );
  }, [data]);

  const [isShow, setIsShow] = useState(false);

  const [selectedYear, setSelectedYear] = useState("");

  let filteredExpenses = [];
  // let isShow = true;

  const toggleShow = () => {
    // isShow = !isShow;            // NEVER UPDATE THE STATE MUTABLY
    setIsShow(!isShow);
  };

  const onDelete = async (id) => {
    // const position = expenses.findIndex((exp) => exp.id === id);
    // expenses.splice(position, 1);

    try {
      const response = await fetch(`http://localhost:3030/expenses/${id}`, {
        method: "DELETE",
      });
      setExpenses((oldExpenses) => oldExpenses.filter((exp) => exp.id !== id));
    } catch (err) {
      console.error(err);
    }
  };

  const onAdd = async (newExpense) => {
    try {
      const response = await fetch("http://localhost:3030/expenses", {
        method: "POST",
        body: JSON.stringify(newExpense),
        headers: {
          "Content-Type": "application/json",
        },
      });

      const expense = await response.json();
      setExpenses((oldExpenses) => [newExpense, ...oldExpenses]);
    } catch (err) {
      console.log(err);
    }
    toggleShow();
  };

  const onYearSelect = (selYear) => {
    setSelectedYear(selYear);
  };

  if (selectedYear === "") {
    filteredExpenses = expenses.slice(0);
  } else {
    filteredExpenses = expenses.filter(
      (exp) => exp.createdAt.getFullYear().toString() === selectedYear
    );
  }

  return (
    <div>
      <h1 className="text-center display-4">My Expenses</h1>

      <div className="row mb-3">
        <div className="col-4 offset-4">
          <div className="d-grid">
            <button onClick={toggleShow} className="btn btn-dark">
              Show Form
            </button>
          </div>
        </div>
        <div className="col-4">
          <ExpenseFilter
            onYearSelect={onYearSelect}
            selectedYear={selectedYear}
          />
        </div>
      </div>

      {isShow && <AddExpense onClose={toggleShow} onAdd={onAdd} />}

      <div className="row">
        {filteredExpenses.map((expense) => (
          <ExpenseItem
            id={expense.id}
            title={expense.title}
            amount={expense.amount}
            createdAt={expense.createdAt}
            onDelete={onDelete}
            key={expense.id}
          />
        ))}
      </div>
    </div>
  );
};

export default Expenses;
