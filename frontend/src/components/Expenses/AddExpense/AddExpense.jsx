import classes from "./AddExpense.module.css";
import { useState } from "react";
import { v4 } from "uuid";

const AddExpense = ({ onClose, onAdd }) => {
  const [title, setTitle] = useState("");
  const [amount, setAmount] = useState(0);
  const [createdAt, setCreatedAt] = useState("");

  console.log("ADD EXPENSE RENDER");

  const submitHandler = (e) => {
    e.preventDefault();

    const newExpense = {
      id: v4(),
      title,
      amount: Number(amount),
      createdAt: new Date(createdAt),
    };
    onAdd(newExpense);
  };

  return (
    <div className={classes["backdrop"]}>
      <div className={classes["my-dialog"]}>
        <h1 className="text-center">Add Expense</h1>
        <form className="m-2" onSubmit={submitHandler}>
          {/* title */}
          <div className="form-floating mb-3">
            <input
              type="text"
              className="form-control"
              name="title"
              placeholder=""
              onChange={(e) => setTitle(e.target.value)}
              value={title}
            />
            <label htmlFor="title">Title</label>
          </div>

          {/* amount */}
          <div className="form-floating mb-3">
            <input
              type="number"
              min="0.5"
              step="0.5"
              className="form-control"
              name="amount"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
              placeholder=""
            />
            <label htmlFor="amount">Amount</label>
          </div>

          {/* date : createdAt */}
          <div className="form-floating mb-3">
            <input
              type="date"
              className="form-control"
              name="createdAt"
              min="2022-04-01"
              max="2024-03-31"
              value={createdAt}
              onChange={(e) => setCreatedAt(e.target.value)}
              placeholder=""
            />
            <label htmlFor="createdAt">Date</label>
          </div>

          {/* buttons */}
          <div className="row">
            <div className="col-6">
              <div className="d-grid">
                <button type="submit" className="btn btn-secondary">
                  Add
                </button>
              </div>
            </div>
            <div className="col-6">
              <div className="d-grid">
                <button
                  type="button"
                  className="btn btn-light"
                  onClick={onClose}
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddExpense;
