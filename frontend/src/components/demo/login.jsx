import { useRef, useState } from "react";

const Login = () => {
  const [password, setPassword] = useState("");

  const [errors, setErrors] = useState([]);

  const userInputRef = useRef();

  const submitHandler = (e) => {
    e.preventDefault();

    console.log("Username : ", userInputRef.current.value);
    console.log("Password : ", password);
  };

  return (
    <div className="row">
      <div className="col-6 offset-3">
        <h1 className="text-center">Login Form</h1>
        <form onSubmit={submitHandler}>
          {/* username - uncontrolled*/}
          <div className="form-floating mb-3">
            <input
              type="text"
              className="form-control"
              name="username"
              id="username"
              placeholder=""
              ref={userInputRef}
            />
            <label htmlFor="username">Username</label>
          </div>

          {/* password - controlled*/}
          <div className="form-floating mb-3">
            <input
              type="password"
              className="form-control"
              name="password"
              id="password"
              placeholder=""
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <label htmlFor="password">Password</label>
          </div>

          {errors.map((error) => {
            return <p key={error}>{error}</p>;
          })}
          {/* button */}
          <button className="btn btn-primary" type="submit">
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
