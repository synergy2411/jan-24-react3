import React from "react";

function Hoc(WrappedComp) {
  return class EnhancedComp extends React.Component {
    state = {
      counter: 0,
    };

    increase = () => this.setState({ counter: this.state.counter + 1 });

    render() {
      return (
        <WrappedComp counter={this.state.counter} increase={this.increase} />
      );
    }
  };
}

export default Hoc;
