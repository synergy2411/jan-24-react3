import Hoc from "./Hoc";

function CounterOne(props) {
  return (
    <button className="btn btn-primary" onClick={props.increase}>
      {props.counter}
    </button>
  );
}

export default Hoc(CounterOne);
