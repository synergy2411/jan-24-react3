import React, { Fragment } from "react";

function Basic() {
  let arr = [];
  let username = "John Doe";

  return (
    <Fragment>
      <p>Basic Component loaded.</p>
      <p>Second Paragraph</p>
    </Fragment>
  );
}

export default Basic;

// <div>
//     <ul>
//         <li>
//             <a href="home">Home</a>
//         </li>
//         <li>
//             <a href="about">About</a>
//         </li>
//     </ul>
// </div>
