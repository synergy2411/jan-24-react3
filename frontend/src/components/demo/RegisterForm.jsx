import { useForm } from "react-hook-form";

function Register() {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm();

  const submitHandler = (data) => {
    console.log(data);
  };
  return (
    <form onSubmit={handleSubmit(submitHandler)}>
      <div className="form-floating mb-3">
        <input
          type="text"
          className="form-control"
          {...register("username", { required: true })}
        />
        <label htmlFor="username">Username</label>
        {errors.username && <p>Username is mandatory field</p>}
      </div>

      {/* <div className="form-check">
            <input className="form-check-input" type="checkbox" value="" id="" />
            <label className="form-check-label" for=""> Default checkbox </label>
          </div> */}
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          {...register("gender", { required: true })}
        />
        <label className="form-check-label" htmlFor="">
          Gender
        </label>
        {errors.gender && <p>Gender is mandatory field</p>}
      </div>

      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
}

export default Register;
