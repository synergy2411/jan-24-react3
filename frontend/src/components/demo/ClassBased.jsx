import React from "react";

class ClassBased extends React.Component {
  state = {
    toggle: false,
    todos: [],
  };

  constructor(props) {
    super(props);
    console.log("CONSTRUCTOR");
  }

  toggleHandler = () => {
    this.setState({
      toggle: !this.state.toggle,
    });
  };

  componentDidMount() {
    console.log("COMPONENT DID MOUNT");
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((response) => response.json())
      .then((todos) => this.setState({ todos }))
      .catch(console.error);
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("SHOULD COMPONENT UPDATE", nextProps, nextState);
    return nextProps.isShow !== this.props.isShow;
  }

  componentDidUpdate() {
    console.log("COMPONENT DID UPDATE");
  }

  componentWillUnmount() {
    console.log("COMPONENT WILL UNMOUNT");
  }

  render() {
    console.log("RENDER");

    // this.setState({
    //   toggle: !this.state.toggle,
    // });

    return (
      <div>
        <h1>Class Based Component Loaded.</h1>

        <button className="btn btn-primary" onClick={this.toggleHandler}>
          Toggle
        </button>
        {this.state.toggle && <p>This paragraph will be toggle</p>}

        {this.props.isShow && <p>Show Props Demonstration</p>}
        <hr />
        <ul>
          {this.state.todos.map((todo) => (
            <li key={todo.id}>{todo.title}</li>
          ))}
        </ul>
      </div>
    );
  }
}

export default ClassBased;

// function FunctionalComp(props) {
//   return (
//     <div>
//       <h1>Functional Component</h1>
//     </div>
//   );
// }
