import React from "react";

function CallBackChildDemo({ toggle, dummyFn }) {
  console.log("Callback Child Component");
  return (
    <>
      <h3>CallBackChildDemo</h3>
      <button className="btn btn-light" onClick={dummyFn}>
        Call Dummy
      </button>
      {toggle && <p>This paragraph will toggle</p>}
    </>
  );
}

export default React.memo(CallBackChildDemo);

// prevProps.toggle === currProps.toggle
// - true : does not re-render the component
// - false : re-render the component

// prevProps.dummyFn === currProps.dummyFn
// - true : does not re-render the component
// - false : re-render the component
