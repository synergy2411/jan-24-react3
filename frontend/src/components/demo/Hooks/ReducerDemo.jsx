import { useReducer } from "react";

function reducer(state, action) {
  if (action.type === "INCREMENT") {
    return {
      ...state,
      counter: state.counter + 1,
    };
  } else if (action.type === "DECREMENT") {
    return {
      ...state,
      counter: state.counter - 1,
    };
  } else if (action.type === "ADD_COUNTER") {
    return {
      ...state,
      counter: state.counter + action.payload,
    };
  } else if (action.type === "SUBTRACT_COUNTER") {
    return {
      ...state,
      counter: state.counter - action.payload,
    };
  } else if (action.type === "STORE_RESULT") {
    return {
      ...state,
      result: [state.counter, ...state.result],
    };
  }
  return state;
}

function ReducerDemo() {
  const [{ counter, result }, dispatch] = useReducer(reducer, {
    counter: 0,
    result: [],
  });

  return (
    <>
      <h1>Reducer Demo</h1>
      <p className="display-4 text-center">Counter : {counter}</p>
      <div className="btn-group mb-3">
        <button
          className="btn btn-primary"
          onClick={() => dispatch({ type: "INCREMENT" })}
        >
          Increase
        </button>
        <button
          className="btn btn-secondary"
          onClick={() => dispatch({ type: "DECREMENT" })}
        >
          Decrease
        </button>
        <button
          className="btn btn-warning"
          onClick={() => dispatch({ type: "ADD_COUNTER", payload: 10 })}
        >
          Add (10)
        </button>
        <button
          className="btn btn-dark"
          onClick={() => dispatch({ type: "SUBTRACT_COUNTER", payload: 5 })}
        >
          Subtract (5)
        </button>
      </div>
      <br />
      <div className="row">
        <div className="col-4 offset-4">
          <div className="d-grid">
            <button
              className="btn btn-info"
              onClick={() => dispatch({ type: "STORE_RESULT" })}
            >
              Store
            </button>
          </div>
          <ul>
            {result.map((r, i) => (
              <li key={i}>{r}</li>
            ))}
          </ul>
        </div>
      </div>
    </>
  );
}

export default ReducerDemo;
