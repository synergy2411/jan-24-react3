import React, { useState, useCallback, useMemo } from "react";
import CallBackChildDemo from "./CallBackChildDemo";

export default function CallbackDemo() {
  const [toggle, setToggle] = useState(true);
  const [counter, setCounter] = useState(0);

  const dummyFn = useCallback(
    () => console.log("Counter : ", counter),
    [counter]
  ); // xixiooox

  const friends = useMemo(() => ["Monica", "Joe", "Chandler"], []);

  const demoFn = useMemo(() => () => console.log("Demo Function"), []);

  console.log("Callback Component");

  return (
    <>
      <h1>CallbackDemo</h1>
      <button className="btn btn-primary" onClick={() => setToggle(!toggle)}>
        Toggle
      </button>
      <button
        className="btn btn-secondary"
        onClick={() => setCounter(counter + 1)}
      >
        Increase
      </button>
      <CallBackChildDemo toggle={true} dummyFn={dummyFn} friends={friends} />
    </>
  );
}
