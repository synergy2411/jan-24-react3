import { useEffect, useState } from "react";

function EffectDemo() {
  const [toggle, setToggle] = useState(true);
  const [counter, setCounter] = useState(0);
  const [serachTerm, setSearchTerm] = useState("");
  const [repos, setRepos] = useState([]);

  //   useEffect(() => {
  //     console.log("Effect works!");
  //   }, []);

  //   useEffect(() => {
  //     console.log("Effect works!");
  //   }, [counter]);

  //   useEffect(() => {
  //     console.log("Effect works!");
  //     return () => console.log("Clean Up");
  //   }, [counter]);

  useEffect(() => {
    const fetchRepos = async () => {
      try {
        const response = await fetch(
          `https://api.github.com/users/${serachTerm}/repos`
        );
        const repos = await response.json();
        setRepos(repos);
      } catch (err) {
        console.error(err);
      }
    };

    let timer = null;
    if (serachTerm !== "") {
      timer = setTimeout(() => {
        fetchRepos();
      }, 1500);
    }

    return () => {
      clearTimeout(timer);
    };
  }, [serachTerm]);

  return (
    <div>
      <h1>Effect Demo</h1>
      <button className="btn btn-primary" onClick={() => setToggle(!toggle)}>
        Toggle
      </button>
      <button
        className="btn btn-secondary"
        onClick={() => setCounter(counter + 1)}
      >
        {counter}
      </button>
      {toggle && <p>This paragraph will toggle</p>}

      <input
        type="text"
        value={serachTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <ul>
        {repos.map((repo) => (
          <li key={repo.id}> {repo.name}</li>
        ))}
      </ul>
    </div>
  );
}

export default EffectDemo;
