import React from "react";
import AuthContext from "../../../context/auth-context";

class Header extends React.Component {
  render() {
    return (
      <AuthContext.Consumer>
        {(context) => {
          return (
            <div>
              <h1>Header Component</h1>
              {context.isLoggedIn && (
                <button
                  className="btn btn-outline-danger"
                  onClick={() => context.setIsLoggedIn(false)}
                >
                  Logout
                </button>
              )}
            </div>
          );
        }}
      </AuthContext.Consumer>
    );
  }
}
export default Header;

// export default function Header() {
//   return (
//     <AuthContext.Consumer>
//       {(context) => {
//         return (
//           <div>
//             <h1>Header Component</h1>
//             {context.isLoggedIn && (
//               <button
//                 className="btn btn-outline-danger"
//                 onClick={() => context.setIsLoggedIn(false)}
//               >
//                 Logout
//               </button>
//             )}
//           </div>
//         );
//       }}
//     </AuthContext.Consumer>
//   );
// }
