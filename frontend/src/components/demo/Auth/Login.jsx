import React, { useContext } from "react";
import AuthContext from "../../../context/auth-context";

export default function Login() {
  const context = useContext(AuthContext);

  const loginHandler = () => {
    // fetch call to backend server
    // Verify the user
    context.setIsLoggedIn(true);
    // context.setToken(token)
  };

  return (
    <div>
      <h1>Login</h1>
      <p>The User is {context.isLoggedIn ? "" : "NOT"} loggedIn</p>

      {/* username */}
      {/* password */}

      <button className="btn btn-primary" onClick={loginHandler}>
        Login
      </button>
    </div>
  );
}
