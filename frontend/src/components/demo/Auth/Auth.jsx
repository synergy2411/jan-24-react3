import React, { useContext } from "react";
import Login from "./Login";
import Header from "./Header";
import AuthContext from "../../../context/auth-context";

export default function Auth() {
  const context = useContext(AuthContext);
  return (
    <div>
      <Header />
      {!context.isLoggedIn && <Login />}
    </div>
  );
}
