import { useState, useEffect } from "react";

function useFetch(url, endpoint) {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`${url}/${endpoint}`);
        let data = await response.json();
        setData(data);
      } catch (err) {
        console.error(err);
      }
    };
    fetchData();
  }, []);
  return [data, setData];
}

export default useFetch;
