import { useNavigate, Form, useSubmit } from "react-router-dom";

function CourseForm({ course }) {
  const navigate = useNavigate();
  const submitForm = useSubmit();

  const submitHandler = (e) => {
    e.preventDefault();
    let options = {};
    if (course) {
      options = {
        method: "PATCH",
        action: `/courses/${course.id}/edit`,
      };
    } else {
      options = { method: "POST", action: "/courses/new" };
    }
    return submitForm(e.currentTarget, options);
  };
  return (
    <Form onSubmit={submitHandler}>
      {/* title */}
      <div className="form-floating mb-3">
        <input
          type="text"
          className="form-control"
          name="title"
          id="title"
          placeholder=""
          defaultValue={course ? course.title : ""}
        />
        <label htmlFor="title">Title</label>
      </div>

      {/* duration */}
      <div className="form-floating mb-3">
        <input
          type="number"
          className="form-control"
          name="duration"
          id="duration"
          placeholder=""
          defaultValue={course ? course.duration : ""}
        />
        <label htmlFor="duration">Duration</label>
      </div>

      {/* author */}
      <div className="form-floating mb-3">
        <input
          type="text"
          className="form-control"
          name="author"
          id="author"
          placeholder=""
          defaultValue={course ? course.author : ""}
        />
        <label htmlFor="author">Author</label>
      </div>

      {/* button */}
      <div className="row">
        <div className="col-6">
          <div className="d-grid">
            <button type="submit" className="btn btn-primary">
              {course ? "Edit" : "Add"}
            </button>
          </div>
        </div>
        <div className="col-6">
          <div className="d-grid">
            <button
              className="btn btn-secondary"
              type="button"
              onClick={() => navigate("/courses")}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </Form>
  );
}

export default CourseForm;
