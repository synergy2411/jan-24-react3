import { NavLink } from "react-router-dom";

function Header() {
  return (
    <header className="text-center">
      <nav>
        <ul className="nav nav-tabs">
          <li className="nav-item">
            <NavLink
              className={({ isActive }) =>
                isActive ? "nav-link active" : "nav-link"
              }
              to="/"
            >
              Home
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              className={({ isActive }) =>
                isActive ? "nav-link active" : "nav-link"
              }
              to="/courses"
            >
              Courses
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
