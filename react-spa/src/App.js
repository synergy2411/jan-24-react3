import React from "react";

import { createBrowserRouter, RouterProvider } from "react-router-dom";
import HomePage from "./pages/Home/Home";
import CoursesPage, { loader as CoursesLoader } from "./pages/Courses/Courses";
import RootPage from "./pages/Root/Root";
import ErrorPage from "./pages/Error/Error";
import CourseDetailPage, {
  loader as CourseDetailLoader,
  action as CourseDetailAction,
} from "./pages/CourseDetail/CourseDetail";
import NewCoursePage, {
  action as NewCourseAction,
} from "./pages/NewCourse/NewCourse";
import CourseEditPage, {
  loader as CourseEditLoader,
  action as CourseEditAction,
} from "./pages/CourseEdit/CourseEdit";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootPage />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: "/courses",
        element: <CoursesPage />,
        loader: CoursesLoader,
      },
      {
        path: "/courses/:courseId",
        element: <CourseDetailPage />,
        loader: CourseDetailLoader,
        action: CourseDetailAction,
      },
      {
        path: "/courses/new",
        element: <NewCoursePage />,
        action: NewCourseAction,
      },
      {
        path: "/courses/:courseId/edit",
        element: <CourseEditPage />,
        loader: CourseEditLoader,
        action: CourseEditAction,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router}></RouterProvider>;
}

export default App;
