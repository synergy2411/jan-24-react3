import {
  Form,
  redirect,
  useLoaderData,
  json,
  useNavigate,
  useParams,
} from "react-router-dom";

function CourseDetailPage() {
  const course = useLoaderData();
  const params = useParams();

  const navigate = useNavigate();
  const editCourseHandler = () => {
    navigate(`/courses/${params.courseId}/edit`);
  };
  return (
    <>
      <div className="row">
        <div className="col-6 offset-3">
          <div className="card">
            <div className="card-header">
              <h5>{course.title.toUpperCase()}</h5>
            </div>
            <div className="card-body">
              <p>Duration : {course.duration} Hrs</p>
              <p>Author : {course.author}</p>
              <div className="float-end">
                <Form method="DELETE">
                  <button type="submit" className="btn btn-outline-danger">
                    Delete
                  </button>
                  <button
                    className="btn btn-secondary"
                    onClick={editCourseHandler}
                    type="button"
                  >
                    Edit
                  </button>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CourseDetailPage;

export async function action({ request, params }) {
  console.log("Method :", request.method);
  const resp = await fetch(`http://localhost:3030/courses/${params.courseId}`, {
    method: request.method,
  });
  if (!resp.ok) {
    throw json(
      { message: "Unable to delete course for ID " + params.courseId },
      {
        status: 401,
      }
    );
  } else {
    return redirect("/courses");
  }
}

export async function loader({ request, params }) {
  const response = await fetch(
    `http://localhost:3030/courses/${params.courseId}`
  );

  if (!response.ok) {
    throw { message: "uanble to fetch the course for id - " + params.courseId };
  } else {
    return response;
  }
}
