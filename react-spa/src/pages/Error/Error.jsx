import { useRouteError } from "react-router-dom";
import Header from "../../components/Header/Header";

function ErrorPage() {
  const error = useRouteError();
  console.log(error);
  let message = "Something went wrong";
  if (error) {
    message = error.message;
  }

  return (
    <div className="container">
      <Header />
      <div className="text-center">
        <h1>Error Occured</h1>
        <p>{message}</p>
      </div>
    </div>
  );
}

export default ErrorPage;
