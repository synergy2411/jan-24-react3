import { redirect } from "react-router-dom";
import CourseForm from "../../components/CourseForm/CourseForm";

function NewCoursePage() {
  return (
    <>
      <h1>New Course Page</h1>
      <div className="row">
        <div className="col-6 offset-3">
          <CourseForm />
        </div>
      </div>
    </>
  );
}

export default NewCoursePage;

export async function action({ request }) {
  const formData = await request.formData();

  const newCourse = {
    title: formData.get("title"),
    duration: formData.get("duration"),
    author: formData.get("author"),
  };

  const response = await fetch("http://localhost:3030/courses", {
    method: "POST",
    body: JSON.stringify(newCourse),
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (!response.ok) {
    throw { message: "Unable to post the course" };
  } else {
    return redirect("/courses");
  }
}
