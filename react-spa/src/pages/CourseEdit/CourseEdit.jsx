import { json, redirect, useLoaderData, useParams } from "react-router-dom";
import CourseForm from "../../components/CourseForm/CourseForm";

function CourseEditPage() {
  const course = useLoaderData();

  return (
    <>
      <h1 className="text-center mb-2">Course Edit Page</h1>
      <CourseForm course={course} />
    </>
  );
}

export default CourseEditPage;

export async function action({ request, params }) {
  const formData = await request.formData();
  const editCoures = {
    title: formData.get("title"),
    duration: formData.get("duration"),
    author: formData.get("author"),
  };

  const { courseId } = params;
  const response = await fetch(`http://localhost:3030/courses/${courseId}`, {
    method: request.method,
    body: JSON.stringify(editCoures),
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (!response.ok) {
    throw json({ message: "Unable to edit" }, { status: 404 });
  } else {
    return redirect("/courses");
  }
}

export async function loader({ request, params }) {
  const { courseId } = params;
  const response = await fetch(`http://localhost:3030/courses/${courseId}`);
  if (!response.ok) {
    throw json({ message: "Unable to edit" }, { status: 404 });
  } else {
    return response;
  }
}
