import { Outlet } from "react-router-dom";
import Header from "../../components/Header/Header";

function RootPage() {
  return (
    <div className="container">
      <Header />
      <main>
        <Outlet />
      </main>
    </div>
  );
}

export default RootPage;
