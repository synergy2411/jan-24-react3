import { Link, useLoaderData, useNavigate } from "react-router-dom";

function CoursesPage() {
  const courses = useLoaderData();
  const navigate = useNavigate();

  const addCourseHandler = () => {
    navigate("/courses/new");
  };
  return (
    <>
      <h1 className="text-center mb-3">Courses Pages</h1>

      <div className="row mb-3">
        <div className="col-4 offset-4">
          <div className="d-grid">
            <button className="btn btn-primary" onClick={addCourseHandler}>
              Add Course
            </button>
          </div>
        </div>
      </div>

      <ul className="list-group">
        {courses.map((course) => (
          <li key={course.id} className="list-group-item m-2">
            <Link to={`/courses/${course.id}`}>{course.title}</Link>
          </li>
        ))}
      </ul>
    </>
  );
}

export default CoursesPage;

export async function loader() {
  const response = await fetch("http://localhost:3030/courses");
  if (!response.ok) {
    throw { message: "Unable to fetch courses" };
  } else {
    return response;
  }
}
