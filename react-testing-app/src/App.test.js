import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import App from "./App";

describe("App Component", () => {
  test("should render the h1 element", () => {
    render(<App />);
    const h1Element = screen.queryByText(/App Works/i);
    expect(h1Element).not.toBeNull();
  });

  test("should render paragraph", () => {
    render(<App />);
    const pElement = screen.getByText(/This will be dynamic paragraph/);
    expect(pElement).toBeInTheDocument();
  });

  test("should not render paragraph when button is clicked", async () => {
    render(<App />);

    const btnElement = screen.getByRole("button");
    userEvent.click(btnElement);
    const pElement = await screen.findByText(/This will be dynamic paragraph/);
    expect(pElement).not.toBeInTheDocument();
  });
});
