import { render, screen } from "@testing-library/react";
import Child from "./Child";

describe("Child Component", () => {
  test("should render Child Component", () => {
    render(<Child />);
    const childElement = screen.getByText(/Child/, { exact: false });
    expect(childElement).toBeInTheDocument();
  });

  test("should render the posts as listitem", async () => {
    render(<Child />);
    const listItems = await screen.findAllByRole("listitem");
    expect(listItems).toHaveLength(100);
  });
});
