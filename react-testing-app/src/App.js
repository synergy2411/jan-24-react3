import { useState } from "react";
import "./App.css";
import Child from "./Child/Child";

function App() {
  const [toggle, setToggle] = useState(true);

  return (
    <div className="App">
      <h1>App Works</h1>
      <button onClick={() => setToggle(!toggle)}>Toggle</button>
      {toggle && <p>This will be dynamic paragraph</p>}

      <br />
      <Child />
    </div>
  );
}

export default App;
